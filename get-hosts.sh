#! /bin/bash

set -e

HEAD=/etc/hosts.head
TAIL=/etc/hosts.tail
TEMP=/etc/hosts.temp
HOST=/etc/hosts
BASE="10.5.10.75"
URL="https://${BASE}/gethosts"
DNS="dns.internal.smartadserverapis.com"

# test if we are connected to the vpn before
# and exit before changing anything
ping -c 3 ${BASE}
if [ "$?" -ne 0 ]; then
  exit 1
fi

sudo curl -o ${TEMP} ${URL} -k --header "Host: ${DNS}"
sudo dos2unix ${TEMP}
sudo tail -n +2 ${TEMP} | sudo tee ${HOST}
sudo mv ${HOST} ${TEMP}
sudo head -n -1 ${TEMP} | sudo tee ${HOST}
sudo mv ${HOST} ${TEMP}
cat ${HEAD} ${TEMP} ${TAIL} | sudo tee ${HOST}
sudo rm ${TEMP}
exit 0
