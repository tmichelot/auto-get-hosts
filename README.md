# Update Hosts with systemd timers

# How to install

First you need to create two files that will be used to customise your host file if needed.

```sh
sudo touch /etc/hosts.{head,tail}
```

It's pretty easy to understand, `hosts.head` will be prepended and `hosts.tail` will be appended to the final `/etc/hosts` file.

I recommend you customise your `/etc/hosts.head` file like so (replacing {{ your_hostname_here }} by the hostname of your PC):

```
# Static table lookup for hostnames.
# See hosts(5) for details.

127.0.0.1       localhost
::1             localhost
127.0.1.1       {{ your_hostname_here }}
```

Then you'll want to create the necessary directories to put the service, timer and script files into:

```
mkdir -p .config/systemd/{user,user/scripts}
```

From witing the repository you can do:
```
cp get-hosts.{service,timer} ~/.config/systemd/user/
cp get-hosts.sh ~/.config/systemd/user/scripts/
chmod +x ~/.config/systemd/user/scripts/get-hosts.sh
```

You can now enable and start the service:

```
systemctl --user daemon-reload
systemctl --user enable get-hosts.timer
systemctl --user start get-hosts.timer get-hosts.service
```

You're all set your `/etc/hosts` file shoud get updated every 10 minutes while you'r connected to the vpn! :tada:
